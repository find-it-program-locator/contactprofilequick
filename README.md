This custom-ish module allows inline creation of [Contacts-module](https://drupal.org/project/contacts) contacts to reference with [Inline Entity Form](https://drupal.org/project/inline_entity_form), creating a [decoupled-auth](https://www.drupal.org/project/decoupled_auth) user that is set as the owner of the profile.

Not likely to work for general use, but hoping the functionality can move into [Contacts](https://drupal.org/project/contacts) and/or [Profile](https://drupal.org/project/profile).


## Custom hard-coding

The field name that is referencing contacts is presently hard-coded: `field_findit_contacts`.  It's definitely possible to do this without hard-coding or even having the field configurable; the field value is only in there at all because reverse-engineering the way Inline Entity Form builds its forms might take me a week, if that is the way forward, we can start digging here:

```php
  $form_display = $this->getFormDisplay($entity, $entity_form['#form_mode']);
  $form_display->buildForm($entity, $entity_form, $form_state);
```

But i'd originally imagined we would create a standalone form for adding decoupled-auth user plus profile (that is, a Contacts-module contact), and that still might be the way to go.  But i tried forcing access to be allowed to create profiles first, and to my great surprise that worked, so stuck with the approach.


## Problems to fix

The Inline Entity Form submit handler, `#ief_element_submit`, that i'm using is triggered when the sub-form is closed, not when the overall form is submitted, so abandoned forms end up producing just the users with the contact role, so it shows up in the Contact listing but there is no associated profile, and besides they never should have been created because the person submitting the form evidently changed their mind.
