<?php
// See https://www.drupal.org/docs/8/creating-custom-modules/subscribe-to-and-dispatch-events
// to polish this up properly, and maybe make some edits to that page like specifying
// putting in the src/EventSubscriber directory, and fixing the Class name in the first
// comment?

namespace Drupal\contactprofilequick\EventSubscriber;

use Drupal\profile\Event\ProfileEvents;
use Drupal\profile\Event\ProfileLabelEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class LabelSubscriber.
 *
 * @package Drupal\contactprofilequick\EventSubscriber;
 */
class LabelSubscriber implements EventSubscriberInterface {

  public static function getSubscribedEvents() {
    return [
      ProfileEvents::PROFILE_LABEL => 'reLabel',
    ];
  }

  public function reLabel(ProfileLabelEvent $event) {
    $profile = $event->getProfile();
    if (in_array($profile->bundle(), ['crm_indiv', 'crm_org'], TRUE)) {

      if ($profile->bundle() == 'crm_indiv') {
        // @TODO Use when Contacts updates Name module, see
        // https://gitlab.com/freelygive/drupal/contacts/issues/18
        // $name = \Drupal::service('name.formatter')->format($profile->get('crm_name')->getValue(), 'full');
        $name = $profile->crm_name->given . ' ' . $profile->crm_name->family;
      }
      elseif ($profile->bundle() == 'crm_org') {
        $name = $profile->crm_org_name->value;
      }

      $label = t("@name",
        [
          '@name' => $name,
        ]
      );

      $event->setLabel($label);

    }
  }
}
