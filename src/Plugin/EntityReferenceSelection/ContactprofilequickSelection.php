<?php

namespace Drupal\contactprofilequick\Plugin\EntityReferenceSelection;

use Drupal\Core\Form\FormStateInterface;
use Drupal\profile\Plugin\EntityReferenceSelection\ProfileSelection;

/**
 * The Contact module adds several name fields (crm_name for individuals and
 * the crm_org_name for companies) so it was necessary to adjust the way how
 * the Drupal Autocomplete filter the profile entity.
 *
 * @EntityReferenceSelection(
 *   id = "default:contactprofilequick",
 *   label = @Translation("Contact Profile quick selection"),
 *   entity_types = {"profile"},
 *   group = "default",
 *   weight = 1
 * )
 */
class ContactprofilequickSelection extends ProfileSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $configuration = $this->getConfiguration();
    $target_type = $configuration['target_type'];

    $query = $this->entityTypeManager->getStorage($target_type)->getQuery();
    $group = $query->orConditionGroup()
      //->condition('crm_name.given', $match, 'STARTS_WITH')
      //->condition('crm_name.family', $match, 'STARTS_WITH')
      ->condition('crm_org_name', $match, 'STARTS_WITH')
      ->condition('field_public_email', $match, 'STARTS_WITH');
    $query->condition($group);
    // Add entity-access tag.
    $query->addTag($target_type . '_access');

    // Only display the contacts created by the user that is searching.
    $user = \Drupal::currentUser();
    $roles = $user->getRoles();
    // If the user is not administrator nor manager then just display the
    // contacts created by himself.
//    if (empty(array_intersect($roles, ['administrator', 'manager']))) {
//      $query->condition('crm_created_by', \Drupal::currentUser()->id());
//    }

    // Add the Selection handler for system_query_entity_reference_alter().
    $query->addTag('entity_reference');
    $query->addMetaData('entity_reference_selection_handler', $this);

    // Add the sort option.
    if ($configuration['sort']['field'] !== '_none') {
      $query->sort($configuration['sort']['field'], $configuration['sort']['direction']);
    }

    return $query;
  }

}
