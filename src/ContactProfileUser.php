<?php

namespace Drupal\contactprofilequick;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\profile\Entity\Profile;

class ContactProfileUser {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Checks if a user exists with the given mail.
   *
   * @param string $mail
   *
   * @return bool|\Drupal\user\Entity\User
   */
  public function getUserByMail($mail) {
    $users = $this->entityTypeManager->getStorage('user')->loadByProperties(['mail' => $mail]);

    if (empty($users)) {
      return FALSE;
    }
    return array_shift($users);
  }

  /**
   * Create a user for a profile.
   *
   * @param string $mail The user mail.
   * @param string $role The role for the contact.
   *
   * @return \Drupal\user\UserInterface;
   */
  public function createDecoupledUser($mail = '', $role = 'crm_org') {
    /* @var \Drupal\user\UserInterface $user */
    $user = $this->entityTypeManager->getStorage('user')->create();
    $user->addRole($role);
    $user->set('mail', $mail);
    $user->save();

    return $user;
  }

  /**
   * Create a user for a profile.
   *
   * @param \Drupal\profile\Entity\Profile $profile User profile.
   * @param string $mail The mail of the decoupled user.
   *
   * @return \Drupal\user\UserInterface;
   */
  public function editDecoupledUserMail(Profile $profile, $mail) {
    /* @var \Drupal\user\UserInterface $user */
    $user = $this->entityTypeManager->getStorage('user')->load($profile->getOwnerId());
    $user->set('mail', $mail);
    $user->save();

    return $user;
  }
}
