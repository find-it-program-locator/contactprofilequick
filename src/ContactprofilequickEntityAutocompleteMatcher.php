<?php

namespace Drupal\contactprofilequick;

use Drupal\Core\Entity\EntityAutocompleteMatcher;

/**
 * Matcher class to get autocompletion results for entity reference.
 */
class ContactprofilequickEntityAutocompleteMatcher extends EntityAutocompleteMatcher {

  /**
   * The profiles belongs to the users, so whenever a profile is searched
   * the entity that really needs to be returned is the user and not the profile
   * itself.
   * {@inheritdoc}
   */
  public function getMatches($target_type, $selection_handler, $selection_settings, $string = '') {
    if ($target_type == 'profile') {
      $selection_handler = 'default:contactprofilequick';
      unset($selection_settings['target_bundles']);
    }
    return parent::getMatches($target_type, $selection_handler, $selection_settings, $string);
  }

}
